from flask import Blueprint, render_template,current_app,request


general_error_Blueprint = Blueprint('general_error_handler', __name__)

@general_error_Blueprint.app_errorhandler(Exception)
def handleUnExpectedException(exception):
    current_app.logger.error(exception)
    return render_template('500.html')
