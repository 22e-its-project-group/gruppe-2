import os
# Each Flask web application contains a secret key which used to sign session cookies,.
# urandom provides a pseudo random byte permutation
SECRET_KEY = os.urandom(32)
# Gets the absolute path for the folder where the excuting script is located
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DATABASE = BASE_DIR + '/tmp/database.db'
# Upload folder for images
UPLOAD_FOLDER = './upload'
#File extensions that are allowed for uploading. An "Allow list"
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
MAX_CONTENT_LENGTH = 16 * 1000 * 1000
DEBUG = False
