from flask import Blueprint
from controllers.IndexController import index, no_way

indexBlueprint = Blueprint('index',__name__)

indexBlueprint.route('/', methods=['GET'])(index)
indexBlueprint.route('/no_way', methods=['GET'])(no_way)