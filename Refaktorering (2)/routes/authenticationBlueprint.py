from flask import Blueprint
from controllers.authenticationController import create_user,log_in,log_out

autheticationBlueprint = Blueprint('authentication',__name__)

autheticationBlueprint.route('/create', methods=['GET','POST'])(create_user)
autheticationBlueprint.route('/login', methods=['GET','POST'])(log_in)
autheticationBlueprint.route('logout',methods=['GET','POST'])(log_out)