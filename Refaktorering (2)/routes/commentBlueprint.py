from flask import Blueprint
from controllers.commentController import add_comment

commentBlueprint = Blueprint('comments',__name__)

commentBlueprint.route('/add', methods=['POST'])(add_comment)