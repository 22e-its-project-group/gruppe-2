# Importing the necessary modules and libraries
from flask import Flask, g as appContext,render_template
import sqlite3
from routes.authenticationBlueprint import autheticationBlueprint
from routes.imageBlueprint import imageBlueprint
from routes.commentBlueprint import commentBlueprint
from routes.profileBlueprint import userBlueprint
from routes.indexBlueprint import indexBlueprint
from errorHandlers.authenticationErrorHandler import authentication_error_Blueprint
from errorHandlers.generalErrorHandler import general_error_Blueprint

server = Flask(__name__) 
server.config.from_object('config')
server.jinja_env.autoescape = True  

@server.before_request
def before_request():
    server.db = sqlite3.connect(server.config['DATABASE'])
@server.teardown_request
def teardown_request(exception):
    db = getattr(appContext, 'db', None)
    if db is not None:
        db.close()

# Adding routing
server.register_blueprint(indexBlueprint, url_prefix='/')
server.register_blueprint(autheticationBlueprint, url_prefix='/authentication')
server.register_blueprint(imageBlueprint, url_prefix='/image' )
server.register_blueprint(commentBlueprint, url_prefix='/comment')
server.register_blueprint(userBlueprint, url_prefix='/profile')

# Adding errorhandlers
# A general problem is that exception thrown by the error handlers them self will not be caugth
server.register_blueprint(authentication_error_Blueprint)
server.register_blueprint(general_error_Blueprint)

if __name__ == '__main__': #__name__ variable value is __main__  in the root script
    server.run(host='127.0.0.1', port=5000)


