from sys import abiflags
from flask import session,abort,request,flash,redirect,url_for
from services.entry_service import add_entry

def __userIsAuthenticated():
        return session.get('logged_in')

def add_new_entry():
    if not __userIsAuthenticated():
        abort(401)
        
    title = request.form['title']
    text = request.form['text']

    add_entry(title,text)
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))