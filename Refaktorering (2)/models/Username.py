import re
from exceptions.UsernameError import UsernameError

class Username:
    #What happens if the username is none?
    def __init__(self,username):

        self.__validateThatUsernameIsformattetLikeAnEmailAdresse(username)
        self.__username = username

    def getValue(self):
        return self.__username

    def __validateThatUsernameIsformattetLikeAnEmailAdresse (self,username):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        isValidEmailAdresseFormat = re.fullmatch(regex,username)
        
        if(not isValidEmailAdresseFormat):
            raise UsernameError("username must be formatted as an email adresse")
    
    