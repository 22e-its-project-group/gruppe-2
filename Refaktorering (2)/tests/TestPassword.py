import unittest
import sys
sys.path.insert(0,'../')
from models.Password import Password
from exceptions.PasswordError import PasswordError

#The test here only serves ONLY as examples, and are not extensive
class TestPassword(unittest.TestCase):

    def test_password_should_be_at_least_16_characters_long_PasswordError_exception_should_be_thrown(self):
        #Arrange
        passwordString = 'Pa55Word?!ThatI'

         #Act & assert
        with self.assertRaises(PasswordError):
            name = Password(passwordString)

    def test_password_cannot_be_longer_than_24_characters_PasswordError_exception_should_be_thrown(self):
        #Arrange
        passwordString = 'Pa55Word?!ThatIsHard20245'
    
        #Act & assert
        with self.assertRaises(PasswordError):
            name = Password(passwordString)

    def test_password_should_contain_at_least_one_lowercase_character_PasswordError_exception_should_be_thrown(self):
        #Arrange
        passwordString = 'PA55WORD?!THAYISHARD2024'

        #Act & assert
        with self.assertRaises(PasswordError):
            name = Password(passwordString)

    def test_password_should_contain_at_least_one_uppercase_character_PasswordError_exception_should_be_thrown(self):
        #Arrange
        passwordString = 'pa55word?!tahyishard2024'

        #Act & assert
        with self.assertRaises(PasswordError):
            name = Password(passwordString)     
    
    def test_password_should_contain_at_least_one_digit_character_PasswordError_exception_should_be_thrown(self):
        #Arrange
        passwordString = 'PassWord?!ThatIsHardsosh'

        #Act & assert
        with self.assertRaises(PasswordError):
            name = Password(passwordString) 
    
    def test_password_should_contain_at_least_one_special_character_PasswordError_exception_should_be_thrown(self):
        #Arrange
        passwordString = 'Pa55Word8iThatIsHard2024'

        #Act & assert
        with self.assertRaises(PasswordError):
            name = Password(passwordString) 

if __name__ == '__main__':
    unittest.main()