# Importing the necessary modules and libraries
from flask import Flask, g as appContext
import sqlite3
from routes.authenticationBlueprint import autheticationBlueprint
from flask_talisman import Talisman
from routes.imageBlueprint import imageBlueprint
from routes.commentBlueprint import commentBlueprint
from routes.profileBlueprint import userBlueprint
from routes.indexBlueprint import indexBlueprint


app = Flask(__name__) 
talisman = Talisman(app)
app.config.from_object('config')
app.jinja_env.autoescape = True    

@app.before_request
def before_request():
    app.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    db = getattr(appContext, 'db', None)
    if db is not None:
        db.close()


def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


# Registering the blueprint
app.register_blueprint(indexBlueprint, url_prefix='/')
app.register_blueprint(autheticationBlueprint, url_prefix='/authentication')
app.register_blueprint(imageBlueprint, url_prefix='/image' )
app.register_blueprint(commentBlueprint, url_prefix='/comment')
app.register_blueprint(userBlueprint, url_prefix='/profile')

if __name__ == '__main__': #__name__ variable value is __main__  in the root script
    app.run(host='0.0.0.0', port=3333, ssl_context='adhoc')
