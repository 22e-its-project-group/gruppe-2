# SOME APP første refaktorering - folder og file struktur.  
  
Dette er den første iteration af refaktoringen af SOME appen.  
I denne iteration er kildekoden blevet delt op i flere filer. Dette gør som udgangspunkt koden
mere overskuelig. Derudover er applikationen delt op i tre ansvars områder:  
  
  - Blueprint. Har ansvaret for routing.  
  - Controller. Har ansvaret for at håndterering at alle indkommende forespørgelser.  
  - Service. Har ansvaret for håndtering af alle kald udenfor applikations  domæmnet (Database queries F.eks.)  
  
Konceptuelt er flowet for et request er |Blueprint|->|Controller|->|Service|  


Koden er som udgangspunkt den samme som i den oprindelige kildekode, blot delt op i flere filer.  
Så koden er  stadig rodet og usikker.  
  
Denne [artikel](https://medium.com/@nxtbyt/minimal-flask-application-using-mvc-design-pattern-842845cef703) viser en gennemgang af strukturen.  
  
For at starte appen, kan følgende powershell kommandoer bruges:  
  
1. `python -m venv env`  
2. `./env/Scripts/activate`  
3. `pip install -r requirements.txt`  
4. `flask --app app run`  

  
  

 