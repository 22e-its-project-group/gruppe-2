from flask import render_template, session
from services.image_service import  get_images_for_user

def user_profile():
    userId = session.get('user_id')
    result = get_images_for_user(userId)
    return render_template(
        'profile/profile.html',
        images=result['images'],
        shared_images=result['shared_images'])