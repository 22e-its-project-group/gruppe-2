from flask import request, redirect, session, url_for,render_template,flash
from services.authentication_service import add_new_user ,authenticate_user


def create_user():
    error = None
    if request.method == 'POST':
        if request.form['username'] == "":
            error = 'Username needed'
        elif request.form['password'] == "" or request.form['repassword'] == "":
            error = 'Password needed'
        elif request.form['password'] != request.form['repassword']:
            error = 'Password is not the same as the retyped'
        else:
            result = add_new_user(
                request.form['username'],
                request.form['password'])
            if(result['status']=='ok'):
                flash('Successfully created - You can now login')
                return redirect(url_for('authentication.log_in'))
            else:
                error = result['userMessage']
    return render_template(
        'authentication/create.html'
        ,error=error
        )



def log_in():
     error = None
     if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        result = authenticate_user(username,password)
        if(result['status']=='ok'):
            session['logged_in']=True
            session['user_id']=result['user_id']
            flash('You were logged in')
            return  redirect(url_for('profile.user_profile'))
        else:
            error = result['userMessage']
     return render_template(
        'authentication/login.html',
        error=error
        )


def log_out():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('index.index'))