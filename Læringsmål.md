# Læringsmål:
## Software Sikkerhed:
### Den studerende har viden om: 
- Hvilken betydning programkvalitet har for it-sikkerhed ift.: 
- Trusler mod software 
- Kriterier for programkvalitet 
- Fejlhåndtering i programmer 
- Forståelse for security design principles, herunder: 
	- security by design 
	- privacy by design 
	
### Den studerende kan: 
- Tage højde for sikkerhedsaspekter ved at: 
- Programmere håndtering af forventede og uventede fejl 
- Definere lovlige og ikke-lovlige input data, bl.a. til test 
- Bruge et API og/eller standard biblioteker 
- Opdage og forhindre sårbarheder i programkoder 
- Sikkerhedsvurdere et givet software arkitektur 

### Den studerende kan: 
- Håndtering risikovurdering af programkode for sårbarheder. 
- Håndtere udvalgte krypteringstiltag 

## Introduktion til IT-Sikkerhed
### Den studerende har viden om og forståelse for: 
- Grundlæggende programmeringsprincipper 
- Grundlæggende netværksprotokoller 
- Sikkerhedsniveau i de mest anvendte netværksprotokoller 

### Den studerende kan supportere løsning af sikkerhedsarbejde ved at: 
- Anvende primitive datatyper og abstrakte datatyper 
- Konstruere simple programmer der bruge SQL databaser 
- Konstruere simple programmer der kan bruge netværk 
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik 
- Opsætte et simpelt netværk. 
- Mestre forskellige netværksanalyse tools 
- Læse andres scripts samt gennemskue og ændre i dem 

### Den studerende kan: 
- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

## Sikkerhed i Webapplikation
### Efter fuldførelse af valgfaget har den studerende viden om:
- Anvendelse af sikkerhedsforanstaltninger til webteknologier.
- Sikkerhedsanalyse af webapplikationer.
- Risiko vurdering i forhold til webløsninger.

### Efter fuldførelse af valgfaget kan den studerende:

- Foretage sikkerhedsforanstaltninger omkring webapplikationer.
- Teste sikkerheden af webløsninger.
- Vurdere om en webløsning er sikker i forhold til dens formål.

### Efter fuldførelse af valgfaget kan den studerende:

- Implementere sikkerhedsforanstaltninger i webløsninger.
- Genkende og løse sikkerhedshuller i webapplikationer.
- Vejlede om, samt anvende teknologier der afhjælper sikkerhedsmæssige udfordringerfor webløsninger.
