# Gruppe 2
Thobias, Camilla & Lasse.


## [Projektbeskrivelse](https://gitlab.com/-/ide/project/22e-its-project-group/gruppe-2/tree/main/-/Projektbeskrivelse.md/#)
- Find fejl, udbedre dem, optimer applikation.

### Modeller *(work in progress)*
![Domæne-model](Domæne model.png)

![Objekt-model](https://gitlab.com/22e-its-project-group/gruppe-2/-/raw/main/Objekt_Model.jpg)
