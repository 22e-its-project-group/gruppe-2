# Projektbeskrivelse
[IT Sikkerhed Projekt](https://22e-its-project-group.gitlab.io/22e-its-projekt/)

## Formål
Dette projekts formål er at forbedre gruppens kendskab til [læringsmålene](https://gitlab.com/-/ide/project/22e-its-project-group/gruppe-2/tree/main/-/L%C3%A6ringsm%C3%A5l.md/#).

## Projektorganisationen
Projektet bliver fremstillet af gruppemedlemmerne, [Camilla](https://gitlab.com/cbka), [Thobias](https://gitlab.com/tjse28878) & [Lasse](https://gitlab.com/Lass3728). (Link til gitlabs)
Vi vil udnytte Scrum-strukturens guidelinjer til fuldførelsen af dette projekt. 

## Målgruppe
Målgruppen er til personer som er igang med IT-Sikkerhedsuddannelsen, samt personer med interesse inden for IT-Sikkerhed og har forståelse for de basale elementer i IT-Sikkerhedsstrukturen.

## Projektmål
Projektet tager udgangspunkt i den prædefinerede [webapplikation](https://github.com/Silverbaq/ITSec-ImageSharing) udleveret i faget **Sikkerhed i Webapplikation**, som gruppen ønsker at forbedre ved at udføre *pentesting*, *risikovurderinger* og *kodeoptimeringer* som efterlever *secure by design*.

Under projektets forløb vil andre grupper fra IT-Sikkerhedsuddannelsen 2022 lave *peer-pentesting* på projekterne.

## Succeskriterier
Projektet er en succes hvis de studerende får fuldført projektmålene, men vigtigst af alt, at de studerende har fået god forståelse af læringsmålene.

## Milestones
![Milestones](https://22e-its-project-group.gitlab.io/22e-its-projekt/milep%C3%A6le_20220823.png)

Gruppen vælger at følge milepælene som er blevet sat under projektfremstillingen.

## Projektøkonomi
Hver gruppemedlem er budgetteret med 5 ECTS point hvilket svarer til 140 timer.
