from flask import Blueprint
from controllers.imageController import upload_image,get_image,share_image,unshare_image

imageBlueprint = Blueprint('images',__name__)

imageBlueprint.route('/getimage/<id>', methods=['GET'])(get_image)
imageBlueprint.route('/uploadimage', methods=['GET','POST'])(upload_image)
imageBlueprint.route('/shareimage', methods=['POST'])(share_image)
imageBlueprint.route('/unshareimage', methods=['POST'])(unshare_image)