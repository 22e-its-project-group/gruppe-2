from flask import Blueprint
from controllers.profileController import user_profile

userBlueprint = Blueprint('profile',__name__)

userBlueprint.route('/getprofile', methods=['GET'])(user_profile)