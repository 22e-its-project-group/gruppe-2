from flask import Blueprint, render_template,current_app,request
from exceptions.RepasswordError import RePasswordError
from exceptions.PasswordError import PasswordError
from exceptions.UsernameError import UsernameError
from exceptions.CredentialNomatchErrror import CredentialNomatchErrror
from exceptions.UserTakenError import UserTakenError

authentication_error_Blueprint = Blueprint('authentication_error_handler', __name__)


#Inheritance amongst the authentication exceptions could make this code even cleaner
@authentication_error_Blueprint.app_errorhandler(RePasswordError)
@authentication_error_Blueprint.app_errorhandler(PasswordError)
@authentication_error_Blueprint.app_errorhandler(UsernameError)
@authentication_error_Blueprint.app_errorhandler(UserTakenError)
@authentication_error_Blueprint.app_errorhandler(CredentialNomatchErrror) #Here there are some work for logging error code 401 to be done
def handleAutenticationException(exception):
    current_app.logger.error(exception)
    return __returnTemplateWithErrorMessageFromException(exception)

def __returnTemplateWithErrorMessageFromException(exception):
        return render_template(
            __getTemplateFromWhereExceptionWasRaised(),
            error=str(exception)
            )
def __getTemplateFromWhereExceptionWasRaised():
        requestsPaths = request.path.split('/')
        return  requestsPaths[1]+'/'+requestsPaths[2]+'.html'
    