from flask import current_app
from  exceptions.CredentialNomatchErrror import CredentialNomatchErrror
from exceptions.UserTakenError import UserTakenError

def authenticate_user(user):

    cursor = current_app.db.execute("select password from user where username = '{}'".format(user.getUserName()))
    pass_db = [dict(password=row[0]) for row in cursor.fetchall()]

    if pass_db[0].get('password') is None:
        raise CredentialNomatchErrror("Credentials did not match any user")
    p = pass_db[0].get('password')

    if p == user.getPassword():
        cursor = current_app.db.execute("select id from user where username = '{}'".format(user.getUserName()))
        rows = [dict(id=row[0]) for row in cursor.fetchall()]
        user_id = rows[0].get('id')
        return user_id
    else:
         raise CredentialNomatchErrror("Credentials did not match any user")

def add_new_user(user):
     query = "select username from user where username = " + "'" + str(user.getUserName()) + "'"
     cursor = current_app.db.execute(query)
     u = [dict(password=row[0]) for row in cursor.fetchall()]
     if len(u) == 0:
                current_app.db.execute('insert into user (username, password, token) values (?, ?, ?)',
                             [user.getUserName(), user.getPassword(), ''])
                current_app.db.commit()
                return
     raise UserTakenError("Username already used")


