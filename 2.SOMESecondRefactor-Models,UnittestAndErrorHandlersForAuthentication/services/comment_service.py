from flask import current_app


def add_comment_to_image(image_id,user_id,comment):
    #query = "insert into comments (user_id, image_id, comment) values (:image_id, :user_id, :comment)"
    #cur = current_app.db.cursor()
    #fetched_data = cur.execute(query, {"image_id": image_id, "user_id": user_id, "comment": comment}, )
    #current_app.db.execute("insert into comments (user_id, image_id, comment) values ({}, {}, '{}')".format(user_id, image_id, comment))
    current_app.db.execute("insert into comments (user_id, image_id, comment) values (?, ?, ?)",(user_id, image_id, comment))
    current_app.db.commit()
    
