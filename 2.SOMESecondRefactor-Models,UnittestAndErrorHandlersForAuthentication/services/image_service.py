from flask import current_app, request, render_template
from config import ALLOWED_EXTENSIONS
import base64
import os

def __allowed_file(filename):
        ALLOWED_EXTENSIONS  = current_app.config['ALLOWED_EXTENSIONS']
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def __get_env_dir():
    return current_app.config["BASE_DIR"]
    # return os.path.dirname(os.path.abspath(__file__))

def __blob_to_image(filename, ablob):
    folder = __get_env_dir() + '/static/img/'
    with open(folder + filename, 'wb') as output_file:
        output_file.write(base64.b64decode(ablob))
    return filename

def __is_image_owner(image_id, idOfTheUserWhoOwnsTheImage):
    cur = current_app.db.execute("select user_id from images where id = {}".format(image_id))
    img_user_id = [dict(user_id=row[0]) for row in cur.fetchall()]

    if idOfTheUserWhoOwnsTheImage == img_user_id[0].get('user_id'):
        return True
    return False

def __image_is_shared(image_id,user_id):
    cur = current_app.db.execute(
        "select id from share where image_id = {} and to_id = {}".format(image_id, user_id))
    share = [dict(id=row[0]) for row in cur.fetchall()]

    if len(share) > 0:
        return True
    return False

def upload(user_id,file):

        if file and __allowed_file(file.filename):
            filename = file.filename

            current_app.db.execute('insert into images (image, user_id, filename) values (?, ?, ?)',(base64.b64encode(file.read()), user_id, filename))
            current_app.db.commit()
            return {
                'status':'ok'
            }
        else:
            return {
                'status':'failed',
                'messageForUser':'filetype not allowed '
            }
def get_image_by_id(image_id, user_id):
      if(__is_image_owner(image_id,user_id) or __image_is_shared(image_id,user_id)):
        cursor = current_app.db.execute("select image, filename, user_id from images where id = {} ".format(image_id))
        img = [dict(filename=row[1], image=__blob_to_image(row[1], row[0]), user_id=row[2]) for row in cursor.fetchall()]
        cursor = current_app.db.execute('select id, username from user')
        user = [dict(id=row[0], username=row[1]) for row in cursor.fetchall()]
        cursor = current_app.db.execute("select share.id, user.username from share inner join user on user.id == share.to_id where from_id = {} and share.image_id = {}".format(user_id, image_id))
        share = [dict(id=row[0], username=row[1]) for row in cursor.fetchall()]
        cursor = current_app.db.execute("select user.username, comments.comment from user inner join comments on user.id == comments.user_id where comments.image_id = {}".format(image_id))
        comments = [dict(username=row[0], comment=row[1]) for row in cursor.fetchall()]
        return{
            'status':'ok',
            'image_id':image_id,
            'image': img,
            'usernames': user,
            'shares':share,
            'comments':comments,
            'owner':(img[0].get('user_id')== user_id)
        }
        
def get_images_for_user(user_id):
    cursor = current_app.db.execute("select id, image, filename from images where user_id = '{}'".format(user_id))
    images = [dict(image_id=row[0], image=__blob_to_image(row[2], row[1])) for row in cursor.fetchall()]

    cursor = current_app.db.execute("select images.id, images.image, images.filename from images inner join share on images.id = share.image_id where share.to_id = '{}'".format(user_id))
    shared_images = [dict(image_id=row[0], image=__blob_to_image(row[2], row[1])) for row in cursor.fetchall()]

    return {

        'images':images,
        'shared_images':shared_images
    }

def share_with_user(idOfTheUserWhoOwnsTheImage,idForUserThatTheImageWillBeSharedWith,image_id ):
    if __is_image_owner(image_id, idOfTheUserWhoOwnsTheImage):
            current_app.db.execute("insert into share (image_id, to_id, from_id) values ({}, {}, {})".format(image_id, idForUserThatTheImageWillBeSharedWith,idOfTheUserWhoOwnsTheImage))                                                                                      
            current_app.db.commit()
            return {
                'status':'ok'
            }
    else:
            return {
                'status':'failed'
            }
def unshare_with_user(id_of_user_which_image_should_not_be_shared_with):
    
        current_app.db.execute("delete from share where id = {}".format(id_of_user_which_image_should_not_be_shared_with))
        current_app.db.commit()
            
    
     