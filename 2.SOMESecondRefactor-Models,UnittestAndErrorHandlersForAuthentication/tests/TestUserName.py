import unittest
import sys
sys.path.insert(0,'../')
from models.Username import Username
from exceptions.UsernameError import UsernameError

#The test here only serves ONLY as examples, and are not extensive
class TestUserName(unittest.TestCase):

    def test_username_can_be_an_email_addresse_no_exception_should_be_thrown(self):
        #Arrange
        userNameString = 'test@test.dk'

        #Act & assert
        try:
            username = Username(userNameString)
        except:
            self.fail()
    
    def test_can_prevent_simple_xss_attack_exception_should_be_thrown(self):
        #Arrange
        nameString = '<script>alert()</script>'

         #Act & assert
        with self.assertRaises(UsernameError):
            name = Username(nameString)
    
    def test_can_prevent_simple_sql_injection_attack_exception_should_be_thrown(self):
        #Arrange
        nameString = 'Martin\'OR 1=1--'

         #Act & assert
        with self.assertRaises(UsernameError):
            name = Username(nameString)

    def test_name_as_email_cannot_contain_special_character_exception_should_be_thrown(self):
        #Arrange
        nameString = '<test@test.dk'

        #Act & assert
        with self.assertRaises(UsernameError):
            name = Username(nameString)

    def test_email_domain_must_be_present(self):
        #Arrange
        nameString = 'test@.dk'

        #Act & assert
        with self.assertRaises(UsernameError):
            name = Username(nameString)

if __name__ == '__main__':
    unittest.main()