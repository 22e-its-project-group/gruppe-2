# SOME APP anden refaktorering - modeller og fejlhåndtering til authentication  
  
I denne iteration er det blevet implementeret en user klasse samt de dertilhørende domæne
primitiver. Derudover er der blevet implementeret exceptions til domæne primitiverne, og der er blevet 
tilføjet `error handlers` til håndetering af alle authetication fejl. Samt et par unit tests til at 
understøtte domæne primitiver.
  
1. `python -m venv env`  
2. `./env/Scripts/activate`  
3. `pip install -r requirements.txt`  
4. `py app.py` This depends on which enviroment variable you have used for Python


 