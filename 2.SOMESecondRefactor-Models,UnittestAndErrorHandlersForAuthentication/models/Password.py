import re
from exceptions.RepasswordError import RePasswordError
from exceptions.PasswordError import PasswordError


class Password:
    
    def __init__(self,password):
        self.__validatePasswordStrength(password)
        #At this point the password should probably be hashed.
        self.__password = password

    def getValue(self):
        if(not self.__password):
            raise Exception("Password can only be read once")
        passwordToBeReturned = self.__password
        self.__password = None
        return passwordToBeReturned

    def __validatePasswordStrength(self,password):
        #Password is 16-24 byte. At least 1 lowercase,1 uppercase,1 digit and 1 special character
        passwordPatternRegex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{16,24}$"
        result = re.match(passwordPatternRegex,password)
        if(not result):
            raise PasswordError("Password requirements not met")

    def repassword_check(self,other_password):
        self.__validate_is_password_type(other_password)
        if not self.__password == other_password.__password: #It is okay to acces private members from other objects of same class. This works around the read-once pattern
            raise RePasswordError("Password and repassword must match")

    def __validate_is_password_type(self,other_password):
        if not isinstance(other_password, Password):
            raise TypeError("The password to be compared with, must be of password type")