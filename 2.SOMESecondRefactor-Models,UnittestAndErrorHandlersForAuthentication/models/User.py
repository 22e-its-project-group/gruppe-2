from models.Username import Username
from models.Password import Password

class User:
    def __init__(self,username,password):
        self.__validateTypes(username,password)
        self.__username = username
        self.__password = password

    def getUserName(self):
        return self.__username.getValue()

    def getPassword(self):
        return self.__password.getValue() #Remember, Password types enforces read once pattern

    def repassword_check(self,other_password):
        return self.__password.repassword_check(other_password)

    def __validateTypes(self,username,password):
        if(not isinstance(username,Username)):
            raise Exception("Username type must be used for username")
        if(not isinstance(password,Password)):
            raise Exception("Password type must be used for password")