from flask import request, flash, session,redirect,url_for,render_template
from services.image_service import upload, get_image_by_id, share_with_user, unshare_with_user

def get_image(id):
    
    user_id = session.get('user_id')
    result = get_image_by_id(id,user_id)
    if result['status']=='ok':
        return render_template('image/image.html', imageid=id,image=result['image'],usernames=result['usernames'],shares=result['shares'],comments=result['comments'],owner=result['owner'])
    return redirect(url_for('no_way'))

def upload_image():
    if request.method == 'POST':
        file = request.files['file']
        userId = session.get('user_id')
        result = upload(userId,file)

        if(result['status']=='ok'):
            flash('uploaded image: %s' % file.name)
            return redirect(url_for('profile.user_profile'))
        else:
            flash('filetype not allowed')

    return render_template('image/upload.html')

def share_image():
     if request.method == 'POST':
        image_id = request.form['imageid']
        to_userid = request.form['userid']
        user_id = session.get('user_id')
        result = share_with_user(user_id,to_userid,image_id )
        if(result['status']=='ok'):
            flash('Image shared')
            return redirect(url_for('images.get_image', id=image_id))
        

def unshare_image():
    if request.method == 'POST':
        #TODO Autherization check
        shared_id = request.form['shareduser']
        image_id = request.form['imageid']
        unshare_with_user(shared_id)
        flash('Image unshared')
        return redirect(url_for('images.get_image', id=image_id))
    else:
        return redirect(url_for('no_way'))