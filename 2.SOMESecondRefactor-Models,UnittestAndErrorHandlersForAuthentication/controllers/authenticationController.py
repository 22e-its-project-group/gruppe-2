from flask import request, redirect, session, url_for,render_template,flash
from services.authentication_service import add_new_user ,authenticate_user
from models.User import User
from models.Username import Username
from models.Password import Password
from exceptions.PasswordError import PasswordError
from exceptions.RepasswordError import RePasswordError
from exceptions.UsernameError import UsernameError

def create_user():
    if request.method == 'POST':
        user = User(
            Username(request.form['username']),
            Password(request.form['password'])
            )
        repassword = Password(request.form['repassword'])
        user.repassword_check(repassword)

        add_new_user(user)

        flash('Successfully created - You can now login')
        return redirect(url_for('authentication.log_in'))
        
    return render_template(
        'authentication/create.html'
        )

def log_in():
     if request.method == 'POST':
        user = User(
            Username(request.form['username']),
            Password(request.form['password'])
            )
        user_id = authenticate_user(user)
        session['logged_in']=True
        session['user_id']=user_id
        flash('You were logged in')
        return  redirect(url_for('profile.user_profile'))    
        
     return render_template(
        'authentication/login.html'
        )


def log_out():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('index.index'))