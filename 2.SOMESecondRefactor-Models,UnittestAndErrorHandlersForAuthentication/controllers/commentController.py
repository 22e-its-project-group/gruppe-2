from flask import session,request,redirect,url_for,flash
from services.comment_service import add_comment_to_image

def add_comment():
    if request.method == 'POST':
        # TODO: needs to check for autherization
        image_id = request.form['imageid']
        user_id = session.get('user_id')
        comment = request.form['text']
        add_comment_to_image(image_id,user_id,comment)
        flash('Added comment')

        return redirect(
            url_for(
                'images.get_image', 
                id=image_id)
                )